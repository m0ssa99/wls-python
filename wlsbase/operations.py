import importlib
import json
import re
import struct
from collections import OrderedDict

from whaleshares.utils import compat_bytes
from .account import PublicKey
from .operationids import operations
from .types import (Int16, Uint16, Uint32, Uint64, String, Bytes, Array,
                    PointInTime, Bool, Optional, Map, Id, JsonObj,
                    StaticVariant)

default_prefix = "WLS"

asset_precision = {
    "WLS": 3,
    "VESTS": 6
}


class Operation:
    def __init__(self, op):
        if isinstance(op, list) and len(op) == 2:
            if isinstance(op[0], int):
                self.opId = op[0]
                name = self.get_operation_name_for_id(self.opId)
            else:
                self.opId = operations.get(op[0], None)
                name = op[0]
                if self.opId is None:
                    raise ValueError("Unknown operation")

            # convert method name like feed_publish to class
            # name like FeedPublish
            self.name = self.to_class_name(name)
            try:
                klass = self.get_class(self.name)
            except:  # noqa FIXME(sneak)
                raise NotImplementedError(
                    "Unimplemented Operation %s" % self.name)
            else:
                self.op = klass(op[1])
        else:
            self.op = op
            # class name like FeedPublish
            self.name = type(self.op).__name__
            self.opId = operations[self.to_method_name(self.name)]

    @staticmethod
    def get_operation_name_for_id(_id):
        """ Convert an operation id into the corresponding string
        """
        for key, value in operations.items():
            if value == int(_id):
                return key

    @staticmethod
    def to_class_name(method_name):
        """ Take a name of a method, like feed_publish and turn it into
        class name like FeedPublish. """
        return ''.join(map(str.title, method_name.split('_')))

    @staticmethod
    def to_method_name(class_name):
        """ Take a name of a class, like FeedPublish and turn it into
        method name like feed_publish. """
        words = re.findall('[A-Z][^A-Z]*', class_name)
        return '_'.join(map(str.lower, words))

    @staticmethod
    def get_class(class_name):
        """ Given name of a class from `operations`, return real class. """
        module = importlib.import_module('wlsbase.operations')
        return getattr(module, class_name)

    def __bytes__(self):
        return compat_bytes(Id(self.opId)) + compat_bytes(self.op)

    def __str__(self):
        return json.dumps(
            [self.get_operation_name_for_id(self.opId),
             self.op.json()])


class GrapheneObject(object):
    """ Core abstraction class

        This class is used for any JSON reflected object in Graphene.

        * ``instance.__json__()``: encodes data into json format
        * ``bytes(instance)``: encodes data into wire format
        * ``str(instances)``: dumps json object as string

    """

    def __init__(self, data=None):
        self.data = data

    def __bytes__(self):
        if self.data is None:
            return bytes()
        b = b""
        for name, value in self.data.items():
            if isinstance(value, str):
                b += compat_bytes(value, 'utf-8')
            else:
                b += compat_bytes(value)
        return b

    def __json__(self):
        if self.data is None:
            return {}
        d = {}  # JSON output is *not* ordered
        for name, value in self.data.items():
            if isinstance(value, Optional) and value.isempty():
                continue

            if isinstance(value, String):
                d.update({name: str(value)})
            else:
                d.update({name: JsonObj(value)})
        return d

    def __str__(self):
        return json.dumps(self.__json__())

    def toJson(self):
        return self.__json__()

    def json(self):
        return self.__json__()


class Permission(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            prefix = kwargs.pop("prefix", default_prefix)

            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            # Sort keys (FIXME: ideally, the sorting is part of Public
            # Key and not located here)
            kwargs["key_auths"] = sorted(
                kwargs["key_auths"],
                key=lambda x: repr(PublicKey(x[0], prefix=prefix)),
                reverse=False,
            )
            kwargs["account_auths"] = sorted(
                kwargs["account_auths"],
                key=lambda x: x[0],
                reverse=False,
            )

            accountAuths = Map([[String(e[0]), Uint16(e[1])]
                                for e in kwargs["account_auths"]])
            keyAuths = Map([[PublicKey(e[0], prefix=prefix),
                             Uint16(e[1])] for e in kwargs["key_auths"]])
            super(Permission, self).__init__(
                OrderedDict([
                    ('weight_threshold', Uint32(
                        int(kwargs["weight_threshold"]))),
                    ('account_auths', accountAuths),
                    ('key_auths', keyAuths),
                ]))


class Memo(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            prefix = kwargs.pop("prefix", default_prefix)

            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(Memo, self).__init__(
                OrderedDict([
                    ('from', PublicKey(kwargs["from"], prefix=prefix)),
                    ('to', PublicKey(kwargs["to"], prefix=prefix)),
                    ('nonce', Uint64(int(kwargs["nonce"]))),
                    ('check', Uint32(int(kwargs["check"]))),
                    ('encrypted', Bytes(kwargs["encrypted"])),
                ]))


class Vote(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(Vote, self).__init__(
                OrderedDict([
                    ('voter', String(kwargs["voter"])),
                    ('author', String(kwargs["author"])),
                    ('permlink', String(kwargs["permlink"])),
                    ('weight', Int16(kwargs["weight"])),
                ]))


class Comment(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            meta = ""
            if "json_metadata" in kwargs and kwargs["json_metadata"]:
                if (isinstance(kwargs["json_metadata"], dict)
                        or isinstance(kwargs["json_metadata"], list)):
                    meta = json.dumps(kwargs["json_metadata"])
                else:
                    meta = kwargs["json_metadata"]

            super(Comment, self).__init__(
                OrderedDict([
                    ('parent_author', String(kwargs["parent_author"])),
                    ('parent_permlink', String(kwargs["parent_permlink"])),
                    ('author', String(kwargs["author"])),
                    ('permlink', String(kwargs["permlink"])),
                    ('title', String(kwargs["title"])),
                    ('body', String(kwargs["body"])),
                    ('json_metadata', String(meta)),
                ]))


class Amount:
    def __init__(self, d):
        self.amount, self.asset = d.strip().split(" ")
        self.amount = float(self.amount)

        if self.asset in asset_precision:
            self.precision = asset_precision[self.asset]
        else:
            raise Exception("Asset unknown")

    def __bytes__(self):
        # padding
        asset = self.asset + "\x00" * (7 - len(self.asset))
        amount = round(float(self.amount) * 10 ** self.precision)
        return (struct.pack("<q", amount) + struct.pack("<b", self.precision) +
                compat_bytes(asset, "ascii"))

    def __str__(self):
        return '{:.{}f} {}'.format(self.amount, self.precision, self.asset)


class ExchangeRate(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(ExchangeRate, self).__init__(
                OrderedDict([
                    ('base', Amount(kwargs["base"])),
                    ('quote', Amount(kwargs["quote"])),
                ]))


class WitnessProps(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(WitnessProps, self).__init__(
                OrderedDict([
                    ('account_creation_fee',
                     Amount(kwargs["account_creation_fee"])),
                    ('maximum_block_size',
                     Uint32(kwargs["maximum_block_size"])),
                ]))


class Beneficiary(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(Beneficiary, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('weight', Int16(kwargs["weight"])),
                ]))


class Beneficiaries(GrapheneObject):
    def __init__(self, kwargs):
        super(Beneficiaries, self).__init__(
            OrderedDict([
                ('beneficiaries',
                 Array([Beneficiary(o) for o in kwargs["beneficiaries"]])),
            ]))


class CommentOptionExtensions(StaticVariant):
    """ Serialize Comment Payout Beneficiaries.

    Args:
        beneficiaries (list): A static_variant containing beneficiaries.

    Example:

        ::

            [0,
                {'beneficiaries': [
                    {'account': 'furion', 'weight': 10000}
                ]}
            ]
    """

    def __init__(self, o):
        type_id, data = o
        if type_id == 0:
            data = Beneficiaries(data)
        else:
            raise Exception("Unknown CommentOptionExtension")
        StaticVariant.__init__(self, data, type_id)


########################################################
# Actual Operations
########################################################


class AccountCreate(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            prefix = kwargs.pop("prefix", default_prefix)

            assert len(kwargs["new_account_name"]
                       ) <= 16, "Account name must be at most 16 chars long"

            meta = ""
            if "json_metadata" in kwargs and kwargs["json_metadata"]:
                if isinstance(kwargs["json_metadata"], dict):
                    meta = json.dumps(kwargs["json_metadata"])
                else:
                    meta = kwargs["json_metadata"]
            super(AccountCreate, self).__init__(
                OrderedDict([
                    ('fee', Amount(kwargs["fee"])),
                    ('creator', String(kwargs["creator"])),
                    ('new_account_name', String(kwargs["new_account_name"])),
                    ('owner', Permission(kwargs["owner"], prefix=prefix)),
                    ('active', Permission(kwargs["active"], prefix=prefix)),
                    ('posting', Permission(kwargs["posting"], prefix=prefix)),
                    ('memo_key', PublicKey(kwargs["memo_key"], prefix=prefix)),
                    ('json_metadata', String(meta)),
                ]))


class AccountCreateWithDelegation(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            prefix = kwargs.pop("prefix", default_prefix)

            assert len(kwargs["new_account_name"]
                       ) <= 16, "Account name must be at most 16 chars long"

            meta = ""
            if "json_metadata" in kwargs and kwargs["json_metadata"]:
                if isinstance(kwargs["json_metadata"], dict):
                    meta = json.dumps(kwargs["json_metadata"])
                else:
                    meta = kwargs["json_metadata"]
            super(AccountCreateWithDelegation, self).__init__(
                OrderedDict([
                    ('fee', Amount(kwargs["fee"])),
                    ('delegation', Amount(kwargs["delegation"])),
                    ('creator', String(kwargs["creator"])),
                    ('new_account_name', String(kwargs["new_account_name"])),
                    ('owner', Permission(kwargs["owner"], prefix=prefix)),
                    ('active', Permission(kwargs["active"], prefix=prefix)),
                    ('posting', Permission(kwargs["posting"], prefix=prefix)),
                    ('memo_key', PublicKey(kwargs["memo_key"], prefix=prefix)),
                    ('json_metadata', String(meta)),
                    ('extensions', Array([])),
                ]))


class AccountUpdate(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            prefix = kwargs.pop("prefix", default_prefix)

            meta = ""
            if "json_metadata" in kwargs and kwargs["json_metadata"]:
                if isinstance(kwargs["json_metadata"], dict):
                    meta = json.dumps(kwargs["json_metadata"])
                else:
                    meta = kwargs["json_metadata"]

            owner = Permission(
                kwargs["owner"], prefix=prefix) if "owner" in kwargs else None
            active = Permission(
                kwargs["active"],
                prefix=prefix) if "active" in kwargs else None
            posting = Permission(
                kwargs["posting"],
                prefix=prefix) if "posting" in kwargs else None

            super(AccountUpdate, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('owner', Optional(owner)),
                    ('active', Optional(active)),
                    ('posting', Optional(posting)),
                    ('memo_key', PublicKey(kwargs["memo_key"], prefix=prefix)),
                    ('json_metadata', String(meta)),
                ]))


class ChangeRecoveryAccount(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            super(ChangeRecoveryAccount, self).__init__(
                OrderedDict([
                    ('account_to_recover', String(kwargs["account_to_recover"])),
                    ('new_recovery_account', String(kwargs["new_recovery_account"])),
                    ('extensions', Array([])),
                ]))


class Transfer(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            if "memo" not in kwargs:
                kwargs["memo"] = ""
            super(Transfer, self).__init__(
                OrderedDict([
                    ('from', String(kwargs["from"])),
                    ('to', String(kwargs["to"])),
                    ('amount', Amount(kwargs["amount"])),
                    ('memo', String(kwargs["memo"])),
                ]))


class TransferToVesting(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(TransferToVesting, self).__init__(
                OrderedDict([
                    ('from', String(kwargs["from"])),
                    ('to', String(kwargs["to"])),
                    ('amount', Amount(kwargs["amount"])),
                ]))


class WithdrawVesting(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(WithdrawVesting, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('vesting_shares', Amount(kwargs["vesting_shares"])),
                ]))


class ClaimRewardBalance(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(ClaimRewardBalance, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('reward_steem', Amount(kwargs["reward_steem"])),
                    ('reward_vests', Amount(kwargs["reward_vests"])),
                ]))


class SetWithdrawVestingRoute(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(SetWithdrawVestingRoute, self).__init__(
                OrderedDict([
                    ('from_account', String(kwargs["from_account"])),
                    ('to_account', String(kwargs["to_account"])),
                    ('percent', Uint16((kwargs["percent"]))),
                    ('auto_vest', Bool(kwargs["auto_vest"])),
                ]))

class WitnessUpdate(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            prefix = kwargs.pop("prefix", default_prefix)

            if not kwargs["block_signing_key"]:
                kwargs[
                    "block_signing_key"] = \
                    "STM1111111111111111111111111111111114T1Anm"
            super(WitnessUpdate, self).__init__(
                OrderedDict([
                    ('owner', String(kwargs["owner"])),
                    ('url', String(kwargs["url"])),
                    ('block_signing_key',
                     PublicKey(kwargs["block_signing_key"], prefix=prefix)),
                    ('props', WitnessProps(kwargs["props"])),
                    ('fee', Amount(kwargs["fee"])),
                ]))


class AccountWitnessVote(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(AccountWitnessVote, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('witness', String(kwargs["witness"])),
                    ('approve', Bool(bool(kwargs["approve"]))),
                ]))


class CustomJson(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            if "json" in kwargs and kwargs["json"]:
                if (isinstance(kwargs["json"], dict)
                        or isinstance(kwargs["json"], list)):
                    js = json.dumps(kwargs["json"])
                else:
                    js = kwargs["json"]

            if len(kwargs["id"]) > 32:
                raise Exception("'id' too long")

            super(CustomJson, self).__init__(
                OrderedDict([
                    ('required_auths',
                     Array([String(o) for o in kwargs["required_auths"]])),
                    ('required_posting_auths',
                     Array([
                         String(o) for o in kwargs["required_posting_auths"]
                     ])),
                    ('id', String(kwargs["id"])),
                    ('json', String(js)),
                ]))


class CommentOptions(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            # handle beneficiaries
            extensions = Array([])
            beneficiaries = kwargs.get('beneficiaries')
            if beneficiaries and type(beneficiaries) == list:
                ext_obj = [0, {'beneficiaries': beneficiaries}]
                ext = CommentOptionExtensions(ext_obj)
                extensions = Array([ext])

            super(CommentOptions, self).__init__(
                OrderedDict([
                    ('author', String(kwargs["author"])),
                    ('permlink', String(kwargs["permlink"])),
                    ('max_accepted_payout',
                     Amount(kwargs["max_accepted_payout"])),
                    ('percent_steem_dollars',
                     Uint16(int(kwargs["percent_steem_dollars"]))),
                    ('allow_votes', Bool(bool(kwargs["allow_votes"]))),
                    ('allow_curation_rewards',
                     Bool(bool(kwargs["allow_curation_rewards"]))),
                    ('extensions', extensions),
                ]))

class SocialActionCommentCreate(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            meta = ""
            if "json_metadata" in kwargs and kwargs["json_metadata"]:
                if (isinstance(kwargs["json_metadata"], dict)
                        or isinstance(kwargs["json_metadata"], list)):
                    meta = json.dumps(kwargs["json_metadata"])
                else:
                    meta = kwargs["json_metadata"]

            pod = String(kwargs["pod"]) if "pod" in kwargs else None
            max_accepted_payout = Amount(kwargs["max_accepted_payout"]) if "max_accepted_payout" in kwargs else None
            allow_replies = Bool(kwargs["allow_replies"]) if "allow_replies" in kwargs else None
            allow_votes = Bool(kwargs["allow_votes"]) if "allow_votes" in kwargs else None
            allow_curation_rewards = Bool(kwargs["allow_curation_rewards"]) if "allow_curation_rewards" in kwargs else None
            allow_friends = Bool(kwargs["allow_friends"]) if "allow_friends" in kwargs else None

            super(SocialActionCommentCreate, self).__init__(
                OrderedDict([
                    ('permlink', String(kwargs["permlink"])),
                    ('parent_author', String(kwargs["parent_author"])),
                    ('parent_permlink', String(kwargs["parent_permlink"])),
                    ('pod', Optional(pod)),
                    ('max_accepted_payout', Optional(max_accepted_payout)),
                    ('allow_replies', Optional(allow_replies)),
                    ('allow_votes', Optional(allow_votes)),
                    ('allow_curation_rewards', Optional(allow_curation_rewards)),
                    ('allow_friends', Optional(allow_friends)),
                    ('title', String(kwargs["title"])),
                    ('body', String(kwargs["body"])),
                    ('json_metadata', String(meta)),
                ]))

class SocialActionCommentUpdate(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            meta = Optional(None)
            if "json_metadata" in kwargs and kwargs["json_metadata"]:
                if (isinstance(kwargs["json_metadata"], dict)
                        or isinstance(kwargs["json_metadata"], list)):
                    meta = json.dumps(kwargs["json_metadata"])
                else:
                    if "json_metadata" in kwargs:
                        meta = kwargs["json_metadata"]

            title = kwargs["title"] if "title" in kwargs else None
            body = kwargs["body"] if "body" in kwargs else None

            super(SocialActionCommentUpdate, self).__init__(
                OrderedDict([
                    ('permlink', String(kwargs["permlink"])),
                    ('title', Optional(String(kwargs["title"]))),
                    ('body', Optional(String(kwargs["body"]))),
                    ('json_metadata', meta),
                ]))

class SocialActionCommentDelete(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(SocialActionCommentDelete, self).__init__(
                OrderedDict([
                    ('permlink', String(kwargs["permlink"]))
                ]))
                
class SocialActionClaim(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(SocialActionClaim, self).__init__(
                OrderedDict([
                    ('amount', Amount(kwargs["amount"])),
                    ('to', Optional(String(kwargs["to"]))),
                    ('memo', Optional(String(kwargs["memo"]))),
                ]))
                
class SocialActionClaimtoTip(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(SocialActionClaimtoTip, self).__init__(
                OrderedDict([
                    ('amount', Amount(kwargs["amount"])),
                ]))
                
class SocialActionCommentTip(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
                
            super(SocialActionCommentTip, self).__init__(
                OrderedDict([
                    ('author', String(kwargs["author"])),
                    ('permlink', String(kwargs["permlink"])),
                    ('amount', Amount(kwargs["amount"])),
                    ('memo', Optional(String(kwargs["memo"]))),
                ]))  
                
class SocialActionUserTip(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(SocialActionUserTip, self).__init__(
                OrderedDict([
                    
                    ('amount', Amount(kwargs["amount"])),
                    ('to', Optional(String(kwargs["to"]))),
                    ('memo', Optional(String(kwargs["memo"]))),
                ]))
                
class SocialActionVariant(StaticVariant):
    def __init__(self, o):
        type_id, data = o
        if type_id == 0:
            data = SocialActionCommentCreate(data)
        else:
            if type_id == 1:
                data = SocialActionCommentUpdate(data)
            else:
                if type_id == 2:
                    data = SocialActionCommentDelete(data)
                else:
                    if type_id == 3:
                        data = SocialActionClaim(data)
                    else:
                        if type_id == 4:
                            data = SocialActionClaimtoTip(data)
                        else:
                            if type_id == 5:
                                data = SocialActionUserTip(data)
                            else:
                                if type_id == 6:
                                    data = SocialActionCommentTip(data)
                                else:
                                    raise Exception("Unknown SocialAction")
        StaticVariant.__init__(self, data, type_id)

class SocialAction(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            # handle action
            action = kwargs.get('action')
            if action == None:
                action_obj = kwargs.get('social_action_comment_create')
                action_id = 0
                if action_obj and type(action_obj) == dict:
                    action_id = 0
                else:
                    action_obj = kwargs.get('social_action_comment_update')
                    if action_obj and type(action_obj) == dict:
                        action_id = 1
                    else:
                        action_obj = kwargs.get('social_action_comment_delete')
                        if action_obj and type(action_obj) == dict:
                            action_id = 2
                        else:
                            action_obj = kwargs.get('social_action_claim_vesting_reward')
                            if action_obj and type(action_obj) == dict:
                                action_id = 3
                            else:
                                action_obj = kwargs.get('social_action_claim_vesting_reward_tip')
                                if action_obj and type(action_obj) == dict:
                                    action_id = 4
                                else:
                                    action_obj = kwargs.get('social_action_user_tip')
                                    if action_obj and type(action_obj) == dict:
                                        action_id = 5
                                    else:
                                        action_obj = kwargs.get('social_action_comment_tip')
                                        if action_obj and type(action_obj) == dict:
                                            action_id = 6
                stat_var = [action_id, action_obj]
                action = SocialActionVariant(stat_var)
            else:
                action = SocialActionVariant([action[0], action[1]])

            super(SocialAction, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('action', action),
                ]))
                
class FriendSend(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(FriendSend, self).__init__(
                OrderedDict([
                    ('memo', String(kwargs["memo"])),
                ]))
                
class FriendAccept(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(FriendAccept, self).__init__(
                OrderedDict([
                    
                ]))
                
class FriendReject(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(FriendReject, self).__init__(
                OrderedDict([
                    
                ]))
                
class FriendUnfriend(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            super(FriendUnfriend, self).__init__(
                OrderedDict([
                    
                ]))

class FriendVariant(StaticVariant):
    def __init__(self, o):
        type_id, data = o
        if type_id == 0:
            data = FriendSend(data)
        else:
            if type_id == 1:
                data = FriendCancel(data)
            else:
                if type_id == 2:
                    data = FriendAccept(data)
                else:
                    if type_id == 3:
                        data = FriendReject(data)
                    else:
                        if type_id == 4:
                            data = FriendUnfriend(data)
                        else:
                            raise Exception("Unknown FriendAction")
        StaticVariant.__init__(self, data, type_id)
        
class FriendAction(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            # handle action
            print(kwargs)
            action = kwargs.get('action')
            print (action)
            if action == None:
                action_obj = kwargs.get('friend_action_send_request')
                action_id = 0
                print (action_obj)
                if action_obj and type(action_obj) == dict:
                    action_id = 0
                else:
                    action_obj = kwargs.get('friend_action_cancel_request')
                    if action_obj is not None and type(action_obj) == dict:
                        action_id = 1
                    else:
                        action_obj = kwargs.get('friend_action_accept_request')
                        if action_obj is not None and type(action_obj) == dict:
                            action_id = 2
                        else:
                            action_obj = kwargs.get('friend_action_reject_request')
                            if action_obj is not None and type(action_obj) == dict:
                                action_id = 3
                            else:
                                action_obj = kwargs.get('friend_action_unfriend')
                                if action_obj is not None and type(action_obj) == dict:
                                    action_id = 4
                print (action_id)
                stat_var = [action_id, action_obj]
                action = FriendVariant(stat_var)
            else:
                action = FriendVariant([action[0], action[1]])

            super(FriendAction, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('another', String(kwargs["another"])),
                    ('action', action),
                ]))
                
class AccountActionPodCreate(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            meta = ""
            if "json_metadata" in kwargs and kwargs["json_metadata"]:
                if (isinstance(kwargs["json_metadata"], dict)
                        or isinstance(kwargs["json_metadata"], list)):
                    meta = String(json.dumps(kwargs["json_metadata"]))
                else:
                    meta = String(kwargs["json_metadata"])
                    
            join_fee = Amount(kwargs["join_fee"]) if "join_fee" in kwargs else None
            allow_join = Bool(kwargs["allow_join"]) if "allow_join" in kwargs else None
            
            super(AccountActionPodCreate, self).__init__(
                OrderedDict([
                    ('fee', Amount(kwargs["fee"])),
                    ('join_fee ', Optional(join_fee)),
                    ('json_metadata ', Optional(meta)),
                    ('allow_join  ', Optional(allow_join)),
                    
                    
                ]))
                
class AccountActionTransferToTip(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            super(AccountActionTransferToTip, self).__init__(
                OrderedDict([
                    ('amount', Amount(kwargs["amount"])),
                ]))
              
                
class AccountActionVariant(StaticVariant):
    def __init__(self, o):
        type_id, data = o
        if type_id == 0:
            data = AccountActionPodCreate(data)
        else:
            if type_id == 1:
                data = AccountActionTransferToTip(data)
            else:
                if type_id == 2:
                    data = AccountActionHtlcCreate(data)
                else:
                    if type_id == 3:
                        data = AccountActionHtlcUpdate(data)
                    else:
                        if type_id == 4:
                            data = AccountActionHtlcRedeem(data)
                        else:
                            raise Exception("Unknown AccountAction")
        StaticVariant.__init__(self, data, type_id)
        
        
class AccountAction(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]
            
            # handle action
            action = kwargs.get('action')
            if action == None:
                action_obj = kwargs.get('account_action_create_pod')
                action_id = 0
                if action_obj and type(action_obj) == dict:
                    action_id = 0
                else:
                    action_obj = kwargs.get('account_action_transfer_to_tip')
                    if action_obj and type(action_obj) == dict:
                        action_id = 1
                    else:
                        action_obj = kwargs.get('account_action_htlc_create')
                        if action_obj and type(action_obj) == dict:
                            action_id = 2
                        else:
                            action_obj = kwargs.get('account_action_htlc_update')
                            if action_obj and type(action_obj) == dict:
                                action_id = 3
                            else:
                                action_obj = kwargs.get('account_action_htlc_redeem')
                                if action_obj and type(action_obj) == dict:
                                    action_id = 4
                print (action_id)
                stat_var = [action_id, action_obj]
                action = AccountActionVariant(stat_var)
            else:
                action = AccountActionVariant([action[0], action[1]])

            super(AccountAction, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('action', action),
                ]))
                
class PodActionVariant(StaticVariant):
    def __init__(self, o):
        type_id, data = o
        if type_id == 0:
            data = PodActionJoinRequest(data)
        else:
            if type_id == 1:
                data = PodActionCancelJoinRequest(data)
            else:
                if type_id == 2:
                    data = PodActionAcceptJoinRequest(data)
                else:
                    if type_id == 3:
                        data = PodActionrejectJoinRequest(data)
                    else:
                        if type_id == 4:
                            data = PodActionLeave(data)
                        else:
                            if type_id == 5:
                                data = PodActionKick(data)
                            else:
                                if type_id == 6:
                                    data = PodActionUpdate(data)
                                else:
                                    raise Exception("Unknown PodAction")
        StaticVariant.__init__(self, data, type_id)
        
class PodAction(GrapheneObject):
    def __init__(self, *args, **kwargs):
        if isArgsThisClass(self, args):
            self.data = args[0].data
        else:
            if len(args) == 1 and len(kwargs) == 0:
                kwargs = args[0]

            # handle action
            action = kwargs.get('action')
            if action == None:
                action_obj = kwargs.get('pod_action_join_request')
                action_id = 0
                if action_obj and type(action_obj) == dict:
                    action_id = 0
                else:
                    action_obj = kwargs.get('pod_action_cancel_join_request')
                    if action_obj and type(action_obj) == dict:
                        action_id = 1
                    else:
                        action_obj = kwargs.get('pod_action_accept_join_request')
                        if action_obj and type(action_obj) == dict:
                            action_id = 2
                        else:
                            action_obj = kwargs.get('pod_action_reject_join_request')
                            if action_obj and type(action_obj) == dict:
                                action_id = 3
                            else:
                                action_obj = kwargs.get('pod_action_leave')
                                if action_obj and type(action_obj) == dict:
                                    action_id = 4
                                else:
                                    action_obj = kwargs.get('pod_action_kick')
                                    if action_obj and type(action_obj) == dict:
                                        action_id = 5
                                    else:
                                        action_obj = kwargs.get('pod_action_update')
                                        if action_obj and type(action_obj) == dict:
                                            action_id = 6
                print (action_id)
                stat_var = [action_id, action_obj]
                action = PodActionVariant(stat_var)
            else:
                action = PodActionVariant([action[0], action[1]])

            super(PodAction, self).__init__(
                OrderedDict([
                    ('account', String(kwargs["account"])),
                    ('pod', String(kwargs["pod"])),
                    ('action', action),
                ]))
                
def isArgsThisClass(self, args):
    return len(args) == 1 and type(args[0]).__name__ == type(self).__name__
