default_prefix = "WLS"

known_chains = {
    "WLS": {
        "chain_id": "de999ada2ff7ed3d3d580381f229b40b5a0261aec48eb830e540080817b72866",
        "prefix": "WLS",
        "steem_symbol": "WLS",
        "vests_symbol": "VESTS",
    },
    "TST": {
        "chain_id": "d0c483beb88660f5071662a8e85c89a9311b20c1764ce9e9f880fad0a098a25d",
        "prefix": "TST",
        "steem_symbol":"TESTS",
        "vests_symbol": "VESTS",
    },
}
