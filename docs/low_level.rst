Low Level
~~~~~~~~~

HttpClient
----------

A fast ``urllib3`` based HTTP client that features:

* Connection Pooling
* Concurrent Processing
* Automatic Node Failover

The functionality of ``HttpClient`` is encapsulated by ``Steem`` class. You shouldn't be using ``HttpClient`` directly,
unless you know exactly what you're doing.

.. autoclass:: wlsbase.http_client.HttpClient
   :members:

-------------

wlsbase
---------

SteemBase contains various primitives for building higher level abstractions.
This module should only be used by library developers or people with deep domain knowledge.

**Warning:**
Not all methods are documented. Please see source.

.. image:: https://i.imgur.com/A9urMG9.png

Account
=======

.. automodule:: wlsbase.account
   :members:

--------

Base58
======

.. automodule:: wlsbase.base58
   :members:

--------

Bip38
=====

.. automodule:: wlsbase.bip38
   :members:


--------

Memo
====

.. automodule:: wlsbase.memo
   :members:


--------

Operations
==========

.. automodule:: wlsbase.operations
   :members:


--------

Transactions
============

.. automodule:: wlsbase.transactions
   :members:



--------

Types
=====

.. automodule:: wlsbase.types
   :members:

--------

Exceptions
==========

.. automodule:: wlsbase.exceptions
   :members: