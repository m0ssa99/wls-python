from whaleshares import *
from wlsbase import *
from whaleshares import Steem

nodes = [ 'https://pubrpc.whaleshares.io' ]

wls = Steem(nodes)

# print(wls.get_all_usernames(''));

def test_post(self):
    op1 = operations.SocialAction(
        **{
            "account": "guest123",
            "social_action_comment_create": {
                "permlink": 'just-a-test-post',
                "parent_author": "",
                "parent_permlink": "test",
                "title": "just a test post",
                "body": "test post body",
                "json_metadata": '{"app":"wls_python"}'
            }
        })

    op2 = operations.SocialAction(
        **{
            "account": "guest123",
            "social_action_comment_update": {
                "permlink": 'just-a-test-post',
                "title": "just a test post",
                "body": "test post body",
            }
        })

    op3 = operations.Vote(
                **{
                    'voter': 'guest123',
                    'author': 'wlsuser',
                    'permlink': 'another-test-post',
                    'weight': 10000,
                })

    privateWif = "5K..."
    tx = transactionbuilder.TransactionBuilder(
        None,
        wls,
        None,
        False,
        60)
    tx.appendOps(op1)
    tx.appendWif(privateWif)
    tx.sign()
    tx.broadcast()

test_post(wls)
